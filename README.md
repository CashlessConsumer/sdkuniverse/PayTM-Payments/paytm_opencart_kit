# Paytm Payment plugin for Opencart

## Table of Contents
- [Overview][0]<br/>
- [Prerequisites][1] <br />
- [Available Versions][2]<br />


## Overview
This plugin adds the Paytm payment option on checkout and enables you to accept payment through Paytm (Wallet/Credit Card/Debit Card/Net Banking/UPI)

## Prerequisites

* PHP >= 5.4
* PHP extensions are required - [cURL][2_link]

## Available Versions
For [1.5.x][1_5]<br />
For [2.0.x, 2.1.x & 2.2.x][2_0]<br />
For [2.3.x][2_3]<br />
For [3.0.x][3_0]

## In case of any query, please contact to Paytm.

<!--LINKS-->

<!--topic urls:-->
[0]: #overview
[1]: #prerequisites
[2]: #available-versions
[3]: #available-versions
[1_5]:/Opencart_V1.5.x
[2_0]:/Opencart_V2.0.x
[2_3]:/Opencart_V2.3.x
[3_0]:/Opencart_V3.x


<!--external links:-->
[2_link]: http://php.net/manual/en/book.curl.php

<!--images:-->
