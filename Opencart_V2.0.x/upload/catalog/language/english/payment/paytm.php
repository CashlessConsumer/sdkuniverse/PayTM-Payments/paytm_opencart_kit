<?php
// Heading
$_['heading_title']                 = 'Thank you for shopping with %s .... ';

// Button
$_['button_confirm']                = 'Confirm Order';

// Text
$_['text_title']                    = 'Credit Card / Debit Card (Paytm PG)';
$_['text_response']                 = 'Response from Paytm: %s';

$_['text_success']                  = 'Your payment has been successfully received.';
$_['text_success_wait']             = '<b><span style="color: #FF0000">Please wait...</span></b> whilst we finish processing your order.<br>If you are not automatically re-directed in 10 seconds, please click <a href="%s">here</a>.';

$_['text_failure']                  = 'Your payment has been failed!';
$_['text_failure_wait']             = '<b><span style="color: #FF0000">Please wait...</span></b><br>If you are not automatically re-directed in 10 seconds, please click <a href="%s">here</a>.';

$_['text_applied_coupon_success']   = 'Applied Successfully';
$_['text_applied_coupon_error']     = 'Incorrect Promo Code';
$_['text_reason']                   = ' Reason: ';
$_['text_transaction_id']           = 'Transaction ID: %s';
$_['text_paytm_order_id']           = 'Paytm Order ID: %s';


// Error
$_['error_server_communication']    = 'It seems some issue in server to server communication. Kindly connect with us.';
$_['error_checksum_mismatch']       = 'Security Error. Checksum Mismatched!';
$_['error_amount_mismatch']         = 'Security Error. Amount Mismatched!';
$_['error_invalid_order']           = 'No order found to process. Kindly contact with us.';